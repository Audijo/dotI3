# Systemd
## Install for Systemd
See `systemd/install.sh`

## Activate in Sway config
The `sway-session.target` is triggered by Sway and must be configured in its config.
```
exec_always "systemctl --user import-environment; systemctl --user start sway-session.target"
```
