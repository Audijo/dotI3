#! /bin/env bash

set -x

ln -s ${PWD}/system/mnt-hidrive.mount /etc/systemd/system/
ln -s ${PWD}/system/mnt-hidrive.automount /etc/systemd/system/
systemctl daemon-reload

