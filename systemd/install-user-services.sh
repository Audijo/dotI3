#! /bin/env bash

set -x

ln -s ${PWD}/user/sway-session.target ~/.config/systemd/user/
ln -s ${PWD}/user/kanshi.service ~/.config/systemd/user/
ln -s ${PWD}/user/rclone-hidrive.service ~/.config/systemd/user/
ln -s ${PWD}/user/unison.service ~/.config/systemd/user/
systemctl --user daemon-reload

